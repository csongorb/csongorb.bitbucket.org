var allLinks = document.getElementsByTagName("a");
var allIframes = document.getElementsByTagName("iframe");
var allSpan = document.getElementsByTagName("span");

var obstacles = [];
var rays = [];

function setup() {
	var myCanvas = createCanvas(windowWidth, windowHeight);
	myCanvas.parent("myCanvas");
}

function draw() {

  clear();

  getHtmlElements();

  getRays();
  extendRays();
  adaptRayVisibility();

  drawRays();
  
}

// ========================
// functions

function getHtmlElements(){

  for (var i = 0; i < allLinks.length; i++) {
    obstacles[i] = new Obstacle(allLinks[i].offsetLeft, allLinks[i].offsetTop - document.body.scrollTop, allLinks[i].offsetLeft + allLinks[i].offsetWidth, allLinks[i].offsetTop - document.body.scrollTop + allLinks[i].offsetHeight);
  }
  for (var i = 0; i < allIframes.length; i++) {
    obstacles[i + allLinks.length] = new Obstacle(allIframes[i].offsetLeft, allIframes[i].offsetTop - document.body.scrollTop, allIframes[i].offsetLeft + allIframes[i].offsetWidth, allIframes[i].offsetTop - document.body.scrollTop + allIframes[i].offsetHeight);
  }
  for (var i = 0; i < allSpan.length; i++) {
    obstacles[i + allLinks.length + allIframes.length] = new Obstacle(allSpan[i].offsetLeft, allSpan[i].offsetTop - document.body.scrollTop, allSpan[i].offsetLeft + allSpan[i].offsetWidth, allSpan[i].offsetTop - document.body.scrollTop + allSpan[i].offsetHeight);
  }
}

function getRays(){

  var nrRays = 0;

  for (var i = 0; i < obstacles.length; i++) {

    rays[nrRays + 0] = new Ray(mouseX, mouseY, obstacles[i].x1, obstacles[i].y1);
    rays[nrRays + 1] = new Ray(mouseX, mouseY, obstacles[i].x1, obstacles[i].y2);
    rays[nrRays + 2] = new Ray(mouseX, mouseY, obstacles[i].x2, obstacles[i].y1);
    rays[nrRays + 3] = new Ray(mouseX, mouseY, obstacles[i].x2, obstacles[i].y2);

    rays[nrRays + 0].obstacleID = i;
    rays[nrRays + 1].obstacleID = i;
    rays[nrRays + 2].obstacleID = i;
    rays[nrRays + 3].obstacleID = i;

    nrRays = nrRays + 4;

  }
}

function adaptRayVisibility(){
  for (var i = 0; i < rays.length; i++) {
    collisionRayObstacle(i);
    cutRay2ndHalf(i);
  } 
}

function collisionRayObstacle(_rID){
  this.rID = _rID;
  var oID = rays[rID].obstacleID;
  var r = false;
  var b = 1; 

  // is the first part of a ray colliding with any obstacle?
  for (var i = 0; i < obstacles.length; i++) {
    if (collideLineRect(rays[rID].sx, rays[rID].sy, rays[rID].mx, rays[rID].my, obstacles[i].x1 + b, obstacles[i].y1 + b, obstacles[i].x2 - obstacles[i].x1 - (2*b), obstacles[i].y2 - obstacles[i].y1 - (2*b))){
      rays[rID].mVisible = false;
    }
  }

  // is the 2nd half colliding with itself?
  if (collideLineRect(rays[rID].mx, rays[rID].my, rays[rID].ex, rays[rID].ey, obstacles[oID].x1 + b, obstacles[oID].y1 + b, obstacles[oID].x2 - obstacles[oID].x1 - (2*b), obstacles[oID].y2 - obstacles[oID].y1 - (2*b))){
    rays[rID].eVisible = false;
  }
}

function cutRay2ndHalf(_rID){

  // cut the 2nd-half of a given ray

  this.rID = _rID;
  var oID = rays[rID].obstacleID;

  if (rays[rID].mVisible && rays[rID].eVisible){

    for (var i = 0; i < obstacles.length; i++) {
      if (i != oID){
        if (collideLineRect(rays[rID].mx, rays[rID].my, rays[rID].ex, rays[rID].ey, obstacles[i].x1, obstacles[i].y1, obstacles[i].x2 - obstacles[i].x1, obstacles[i].y2 - obstacles[i].y1)){
          var c = collideLineRect(rays[rID].mx, rays[rID].my, rays[rID].ex, rays[rID].ey, obstacles[i].x1, obstacles[i].y1, obstacles[i].x2 - obstacles[i].x1, obstacles[i].y2 - obstacles[i].y1, true);
          
          var di = dist(rays[rID].mx, rays[rID].my, rays[rID].ex, rays[rID].ey);
          var diT = dist(rays[rID].mx, rays[rID].my, c.top.x, c.top.y);
          var diB = dist(rays[rID].mx, rays[rID].my, c.bottom.x, c.bottom.y);
          var diL = dist(rays[rID].mx, rays[rID].my, c.left.x, c.left.y);
          var diR = dist(rays[rID].mx, rays[rID].my, c.right.x, c.right.y);

          if (c.top.x && c.top.y && (di > diT)){
            rays[rID].ex = c.top.x;
            rays[rID].ey = c.top.y;
            di = diT;
          }
          if (c.bottom.x && c.bottom.y && (di > diB)){
            rays[rID].ex = c.bottom.x;
            rays[rID].ey = c.bottom.y;
            di = diB;
          }
          if (c.left.x && c.left.y && (di > diL)){
            rays[rID].ex = c.left.x;
            rays[rID].ey = c.left.y;
            di = diL;
          }
          if (c.right.x && c.right.y && (di > diR)){
            rays[rID].ex = c.right.x;
            rays[rID].ey = c.right.y;
            di = diR;
          }
        }
      }
    }
  }
}

function extendRays(){

  // extend rays beyond edges

  var minDist = dist(0, 0, windowWidth, windowHeight);
  for (var i = 0; i < rays.length; i++) {
    var dist1 = dist(rays[i].sx, rays[i].sy, rays[i].mx, rays[i].my);
    var factor = minDist / dist1;
    rays[i].ex = rays[i].sx + ((rays[i].mx - rays[i].sx) * factor);
    rays[i].ey = rays[i].sy + ((rays[i].my - rays[i].sy) * factor);
  }

  // cut rays on edges

  for (var i = 0; i < rays.length; i++) {

    var collisionT = false;
    var collisionL = false;
    var collisionR = false;
    var collisionB = false;

    var collisionT = collideLineLine(rays[i].sx, rays[i].sy, rays[i].ex, rays[i].ey, 0, 0, windowWidth, 0);
    var collisionL = collideLineLine(rays[i].sx, rays[i].sy, rays[i].ex, rays[i].ey, 0, 0, 0, windowHeight);
    var collisionR = collideLineLine(rays[i].sx, rays[i].sy, rays[i].ex, rays[i].ey, windowWidth, 0, windowWidth, windowHeight);
    var collisionB = collideLineLine(rays[i].sx, rays[i].sy, rays[i].ex, rays[i].ey, 0, windowHeight, windowWidth, windowHeight);

    if (collisionT){
      var c = collideLineLine(rays[i].sx, rays[i].sy, rays[i].ex, rays[i].ey, 0, 0, windowWidth, 0, true)
    }
    if (collisionL){
      var c = collideLineLine(rays[i].sx, rays[i].sy, rays[i].ex, rays[i].ey, 0, 0, 0, windowHeight, true)
    }
    if (collisionR){
      var c = collideLineLine(rays[i].sx, rays[i].sy, rays[i].ex, rays[i].ey, windowWidth, 0, windowWidth, windowHeight, true);
    }
    if (collisionB){
      var c = collideLineLine(rays[i].sx, rays[i].sy, rays[i].ex, rays[i].ey, 0, windowHeight, windowWidth, windowHeight, true);
    }

    rays[i].ex = c.x;
    rays[i].ey = c.y;
  }
}

function drawRays(){
  for (var i = 0; i < rays.length; i++) {
    rays[i].draw();
  }
}

// ========================
// objects

function Obstacle(_x1, _y1, _x2, _y2) {
	this.x1 = _x1;
	this.y1 = _y1;
	this.x2 = _x2;
	this.y2 = _y2;
}

function Ray(_sx, _sy, _mx, _my) {
  this.sx = _sx;
  this.sy = _sy;
  this.mx = _mx;
  this.my = _my;
  this.mVisible = true; // mid-point visible?
  this.eVisible = true; // 2nd-half visible?
  var obstacleID;
  var ex;
  var ey;
}

Ray.prototype.draw = function() {
  var r = 4;
  stroke (100);
  fill (100);
  if (this.mVisible){
    //stroke (200, 0, 0);
    //fill (200, 0, 0)
    line (this.sx, this.sy, this.mx, this.my);
    if (!this.eVisible){
      ellipse (this.mx, this.my, r, r);
    }
    if (this.eVisible){
      //stroke (0, 200, 0);
      //fill (0, 200, 0);
      line (this.mx, this.my, this.ex, this.ey);
      ellipse (this.ex, this.ey, r, r);
    }
  }
  ellipse (mouseX, mouseY, r, r);
}
